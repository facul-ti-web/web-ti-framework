import React, { useState } from 'react';
import './App.css';

const api = {
  key: "3ee32176fbc4070662893138e0e9dea6",
  base: "https://api.openweathermap.org/data/2.5/"
}

const dateBuilder = (d) => {
  let months = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
  let days = ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"];

  let day = days[d.getDay()];
  let date = d.getDate();
  let month = months[d.getMonth()];
  let year = d.getFullYear();

  return `${day}, ${date} ${month} ${year}`;
}

function App() {
  const [query, setQuery] = useState('');
  const [weather, setWeather] = useState({});
  
  const search = evt => {
    if (evt.key === "Enter") {
      fetch(`${api.base}weather?q=${query}&lang=pt_br&units=metric&APPID=${api.key}`)
        .then(res => res.json())
        .then(result => {
          setWeather(result);
          setQuery('');
          console.log(result);
        })
        .catch(error => {
          console.error('Erro ao buscar dados da API:', error);
        });
    }
  }
  
  return (
    <div className={(typeof weather.main != "undefined") ? ((weather.main.temp > 15) ? 'appHot' : 'appCold') : 'app'}>
        <div className="searchBox">
          <input 
            type="text"
            className="searchBar"
            placeholder="Pesquisar..."
            onChange={e => setQuery(e.target.value)}
            value={query}
            onKeyDown={search}
          />
        </div>
        {(typeof weather.main != "undefined") ? (
        <div className='containerWeather'>
          <div className="locationBox">
            <div className="location">{weather.name}, {weather.sys.country}</div>
            <div className="date">{dateBuilder(new Date())}</div>
          </div>
          <div className="weatherBox">
            <div className="temperature">
              {Math.round(weather.main.temp)}°C
            </div>
            <div className="weather">{weather.weather[0].description}</div>
            <div className="weatherIcon">
              <img src={`https://openweathermap.org/img/wn/${weather.weather[0].icon}@2x.png`} alt="Ícone do clima" />
            </div>
          </div>
        </div>
        ) : ('')}
    </div>
  );
}

export default App;